## Scripts to test for and apply Virginia Tech minimum security standards for Linux machines.

## Implements the following minimum security standards
* Password Age
   * Maximum of 730 days
   * Minimum of 1 day
* Password Complexity
   * Minimum length of 8
   * Atleast 3 character classes (uppercase, lowercase, number, or symbol)
* Firewall
   * Firewall enabled
   * Default drop policy
* Automatic Updates
   * Security updates will be automatically installed on a regular basis

## Instructions
* Requirements
   * Must run as root
   * Git is required to download
   * Python (2 or 3) is required to run the tool
* Download the tool locally onto your server
```
git clone https://version.cs.vt.edu/techstaff/linux-audit.git
```
* Run a compliance report (this does not make any changes to your system)
```
cd linux-audit
sudo ./report.sh
```
* Items highlighted in red are a fail, items highlighted yellow are recommended changes

* Have the script attempt to modify your system to meet requirements
```
cd linux-audit
sudo ./run.sh
```
* Items highlighted in yellow are changes made, any items highlighted in red means the script failed to make the change

**WARNING** This script may enable your firewall service if it is not running.  Make sure there are sufficient rules in place so that you don't lose connectivity.

