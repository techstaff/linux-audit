#!/bin/bash

if (which python3 &> /dev/null);then
  ln -s ansible ansible-playbook &> /dev/null
  PYTHON=$(which python3)
elif (which python &> /dev/null);then
  ln -s ansible2 ansible-playbook &> /dev/null
  PYTHON=$(which python)
else
  echo ERROR: no python found
fi

$PYTHON ansible-playbook -i ./hosts -b report.yml $1 $2 $3

